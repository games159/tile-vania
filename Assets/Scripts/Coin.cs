using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private AudioClip pickupSFX;
    [SerializeField] private int cost;

    private bool wasCollected;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && !wasCollected)
        {
            wasCollected = true;
            FindObjectOfType<GameSession>().AddScore(cost);
            AudioSource.PlayClipAtPoint(pickupSFX, Camera.main.transform.position);
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }
}

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelExit : MonoBehaviour
{
    [SerializeField] private int sceneToLoadIndex;
    [SerializeField] private float loadDelay;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            StartCoroutine(WaitBeforeLoad());
    }

    IEnumerator WaitBeforeLoad()
    {
        yield return new WaitForSeconds(loadDelay);

        FindObjectOfType<ScenePersist>().ResetScenePersist();
        SceneManager.LoadScene(sceneToLoadIndex);
    }
}

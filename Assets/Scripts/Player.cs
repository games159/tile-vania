using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    [SerializeField] private float runSpeed = 5f;
    [SerializeField] private float jumpStrength = 5f;
    [SerializeField] private float climbSpeed = 5f;
    [SerializeField] private Vector2 deathKick = new Vector2(10f, 10f);
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform bulletSpawner;

    private Vector2 moveInput;
    private Rigidbody2D playerRigidbody;
    private Animator playerAnimator;
    private CapsuleCollider2D playerCollider;
    private BoxCollider2D feetCollider;
    private float gravityScaleAtStart;
    private bool isAlive = true;

    private void Update()
    {
        if (!isAlive)
            return;

        Run();
        FlipSprite();
        ClimbLadder();
    }

    private void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerCollider = GetComponent<CapsuleCollider2D>();
        feetCollider = GetComponent<BoxCollider2D>();
        gravityScaleAtStart = playerRigidbody.gravityScale;
    }

    private void Run()
    {
        var velocity = new Vector2(moveInput.x * runSpeed, playerRigidbody.velocity.y);
        playerRigidbody.velocity = velocity;
    }

    private void FlipSprite()
    {
        var playerHasHorizontalSpeed = Mathf.Abs(playerRigidbody.velocity.x) > Mathf.Epsilon;

        if (playerHasHorizontalSpeed)
            transform.localScale = new Vector2(Mathf.Sign(playerRigidbody.velocity.x), 1f);

        playerAnimator.SetBool("isRunning", playerHasHorizontalSpeed);
    }

    private void ClimbLadder()
    {
        if (!playerCollider.IsTouchingLayers(LayerMask.GetMask("Climbing")))
        {
            playerRigidbody.gravityScale = gravityScaleAtStart;
            playerAnimator.SetBool("isClimbing", false);

            return;
        }

        var velocity = new Vector2(playerRigidbody.velocity.x, moveInput.y * climbSpeed);
        playerRigidbody.velocity = velocity;
        playerRigidbody.gravityScale = 0f;

        var playerHasVerticalSpeed = Mathf.Abs(playerRigidbody.velocity.y) > Mathf.Epsilon;
        playerAnimator.SetBool("isClimbing", playerHasVerticalSpeed);
    }

    private void Die()
    {
        isAlive = false;
        playerAnimator.SetTrigger("Dying");
        playerRigidbody.velocity = deathKick;
        FindObjectOfType<GameSession>().ProcessPlayerDeath();
    }

    private void OnMove(InputValue input)
    {
        if (!isAlive)
            return;

        moveInput = input.Get<Vector2>();
    }

    private void OnJump(InputValue input)
    {
        if (!isAlive)
            return;

        if (!feetCollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
            return;

        if (input.isPressed)
            playerRigidbody.velocity += new Vector2(0f, 1f * jumpStrength);
    }

    private void OnFire(InputValue input)
    {
        if (!isAlive)
            return;

        if (input.isPressed)
            Instantiate(bulletPrefab, bulletSpawner.position, bulletPrefab.transform.rotation);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (playerCollider.IsTouchingLayers(LayerMask.GetMask("Enemies", "Hazards")))
            Die();
    }
}

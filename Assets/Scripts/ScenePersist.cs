using UnityEngine;

public class ScenePersist : MonoBehaviour
{
    private void Awake()
    {
        var numberOfScenePersists = FindObjectsOfType<ScenePersist>().Length;

        if (numberOfScenePersists > 1)
            Destroy(gameObject);
        else
            DontDestroyOnLoad(gameObject);
    }

    public void ResetScenePersist()
    {
        Destroy(gameObject);
    }
}
